const app = require('express')();
const admin = require('firebase-admin');
const serviceAccount = require("./private/firebaseprivatekey");
const http = require('http').createServer({

}, app);
const io = require('socket.io')(http);

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://chamberwatch-64aa2.firebaseio.com/"
});

io.use((socket,next) => {
    let token = socket.handshake.query.token;
    if(!token){
        socket.disconnect();
        return next(new Error('not authorized'));
    }
    admin.auth().verifyIdToken(token)
        .then(function(decodedToken) {
            socket.UID = decodedToken.uid;
            return next()
        }).catch(function(error) {
        socket.disconnect();
        return next(new Error('not authorized'));
    });
});

io.on('connection', (socket) => {
    // socket.join('user-' + socket.UID);
    console.log("user joinded: " + socket.UID);

    socket.on('join-votable-room', (votable) =>{
        socket.join('votable-' + votable.votableId);
    });

    socket.on('join-comment-room', (comment) =>{
        socket.join('comment-' + comment.commentId);
    });

    socket.on('add-comment', (req) =>{
       socket.broadcast.to('voteable-' + req.voteableId).emit('add_comment', req.comment)
    });

    socket.on('like-comment', (req) =>{
        socket.broadcast.to('comment-' + req.commentId).emit('like_comment', req.type)
    });
});

http.listen(3000, function(){
    console.log('listening on port 3000');
});
